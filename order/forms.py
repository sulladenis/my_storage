from django import forms
from order.models import Buyer, OrderItem, Order


class BuyerForm(forms.ModelForm):
    class Meta:
        model = Buyer
        fields = ['name']
        widgets = {
            'name': forms.TextInput(
                attrs={
                    'class': 'form-control'
                }
            ),
        }
        labels = {
            'name': 'Добавте или измените покупателя'
        }


class OrderItemForm(forms.ModelForm):
    class Meta:
        model = OrderItem
        fields = ['product', 'quantity']
        labels = {
            'product': 'Наименование товара',
            'quantity': 'Количество',
        }
        widgets = {
            'product': forms.Select(
                attrs={
                    'class': 'form-control',
                }
            ),
            'quantity': forms.NumberInput(
                attrs={
                    'class': 'form-control'
                }
            ),
        }

    def clean(self):
        cleaned_data = super().clean()
        data_quantity = cleaned_data.get('quantity')
        data_product = cleaned_data.get('product')

        if data_quantity > (data_product.quantity_in_stock - data_product.reserve):
            raise forms.ValidationError("На складе столько нету")
        return cleaned_data


class OrderForm(forms.ModelForm):
    class Meta:
        model = Order
        fields = []

