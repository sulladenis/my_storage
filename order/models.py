from django.db import models
from product.models import Product


class Buyer(models.Model):
    name = models.CharField('Покупатель', max_length=30)

    def __str__(self):
        return self.name


class Order(models.Model):
    STATUS_CHOICES = (
        (0, 'Проверка наличия товара'),
        (1, 'Товар передан'),
        (2, 'Заявка не принята'),
    )

    date = models.DateField('Дата заказа', auto_now_add=True)
    buyer = models.ForeignKey(Buyer, null=True, on_delete=models.CASCADE)
    status = models.IntegerField("Статус", choices=STATUS_CHOICES, default=0)

    def __str__(self):
        return f'заяка {self.pk}'

    @property
    def total_price(self):
        set = self.supplyitem_set.all()
        price = 0
        for item in set:
            price += item.total_price_item
        return price



class OrderItem(models.Model):
    product = models.ForeignKey(Product, null=True, on_delete=models.CASCADE)
    quantity = models.PositiveIntegerField('Колличество', default=1)
    order = models.ForeignKey(Order, null=True, on_delete=models.CASCADE)

    def reservation(self):
        self.product.reserve += self.quantity
        self.product.save()

    def accept(self):
        self.product.quantity_in_stock -= self.quantity
        self.product.reserve -= self.quantity
        self.product.save()

