from django.urls import path

from order import views


urlpatterns = [
#    path('buyer/', buyer_create, name='buyer'),
    path('order/', views.order, name='order'),
    path('order_send/', views.order_sent, name='order_send'),
#    path('order/<int:pk>/', order_detail, name='order_detail'),
    path('product_list_order', views.product_list_order, name='product_list_order'),
    path('product_detail_order/<int:pk>/', views.product_detail_order, name='product_detail_order'),
    path('buyer_list', views.buyer_list, name='buyer_list'),
    path('buyer_for_order/<int:pk>/', views.buyer_for_order, name='buyer_for_order'),
]
