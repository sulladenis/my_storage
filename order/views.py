from django.shortcuts import render
from django.shortcuts import redirect
from order.forms import BuyerForm, OrderItemForm, OrderForm
from order.models import Order, Buyer
from product.models import Product


def product_list_order(request):
    products = Product.objects.all()
    return render(request, 'product_list.html', {'products': products})


def product_detail_order(request, pk):
    order_pk = request.session.get('order')
    order = Order.objects.get(pk=order_pk)
    if request.method == 'POST':
        form = OrderItemForm(request.POST)
        if form.is_valid():
            order_item = form.save(commit=False)
            order_item.order = order
            order_item.reservation()
            order_item.save()
            return redirect('order')
    else:
        product = Product.objects.get(pk=pk)
        form = OrderItemForm(initial={'product': product})
    return render(request, 'product_detail_order.html', {'form': form, 'order': order})


def order(request):
    request.session['selector'] = 'from_order'
    if 'order' not in request.session:
        order = Order.objects.create()
        request.session['order'] = order.pk
    else:
        order_pk = request.session.get('order')
        order = Order.objects.get(pk=order_pk)
    order_items = order.orderitem_set.all()
    if request.method == 'POST' and 'buyer_submit' in request.POST:
        form_buyer = BuyerForm(request.POST)
        if form_buyer.is_valid():
            name = form_buyer.cleaned_data['name']
            try:
                buyer = Buyer.objects.get(name=name)
            except Buyer.DoesNotExist:
                buyer = form_buyer.save()
            order.buyer = buyer
            order.save()
            return redirect('order')
    if request.method == 'POST' and 'order_item_submit' in request.POST:
        form_order_item = OrderItemForm(request.POST)
        if form_order_item.is_valid():
            order_item = form_order_item.save(commit=False)
            order_item.order = order
            order_item.reservation()
            order_item.save()
            return redirect('order')
    if order.buyer:
        buyer_name = order.buyer.name
    else:
        buyer_name = None
    form_buyer = BuyerForm(initial={'name': buyer_name})
    form_order_item = OrderItemForm()
    context = {
        'order': order,
        'order_items': order_items,
        'form_buyer': form_buyer,
        'form_order_item': form_order_item,
    }
    return render(request, 'order.html', context)


def order_sent(request):
    order_pk = request.session.get('order')
    order = Order.objects.get(pk=order_pk)
    del request.session['order']
    del request.session['selector']
    return render(request, 'order_sent.html', {'order': order})


def buyer_list(request):
    buyers = Buyer.objects.all()
    link = request.GET.get('link')
    if link == 'nav':
        request.session['selector'] = ''
    return render(request, 'buyer_list.html', {'buyers': buyers})


def buyer_for_order(request, pk):
    order_pk = request.session.get('order')
    order = Order.objects.get(pk=order_pk)
    buyer = Buyer.objects.get(pk=pk)
    if request.method == 'POST':
        form = OrderForm(request.POST, instance=order)
        if form.is_valid():
            order = form.save(commit=False)
            order.buyer = buyer
            order.save()
            return redirect('order')
    else:
        form = OrderForm()
        context = {'form': form, 'buyer': buyer, 'order': order}
        return render(request, 'buyer_for_order.html', context=context)
