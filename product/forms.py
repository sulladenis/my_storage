from django.forms import (
    ModelForm,  TextInput,
    NumberInput, Select,
)
from product.models import Category, Product


class CategoryForm(ModelForm):

    class Meta:
        model = Category
        fields = ['name', 'main', 'parent' ]
        labels = {
            'name': 'Наименование категории',
            'parent':'Родительская категория',
            'main': 'Статус'
        }
        widgets = {
            'name': TextInput(
                attrs={
                    'class': 'form-control',
                }
            ),
            'parent': Select(
                attrs={
                    'class': 'form-control',
                }
            ),
            'main': Select(
                choices=[
                         (True, "Главная"),
                         (False, "Подкатегория")],
                attrs={
                    'class': 'form-control',
                }
            ),
        }



class ProductForm(ModelForm):

    class Meta:
        model = Product
        fields = ['name', 'category', 'price', 'quantity_in_stock', 'vendor_code', 'description']
        labels = {
            'name': 'Наименование продукта',
            'category': 'Категория',
            'quantity_in_stock': 'Начальное количество',

        }
        widgets = {
            'name': TextInput(
                attrs={
                    'class': 'form-control',
                }
            ),
            'category': Select(
                attrs={
                    'class': 'form-control',
                }
            ),
            'price':  NumberInput(
                attrs={
                    'class': 'form-control',
                }
            ),
            'vendor_code': TextInput(
                attrs={
                    'class': 'form-control',
                }
            ),
            'quantity_in_stock': NumberInput(
                attrs={
                    'class': 'form-control',
                }
            ),
            'description': TextInput(
                attrs={
                    'class': 'form-control',
                }
            ),
        }


