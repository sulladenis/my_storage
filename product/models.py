from uuid import uuid4
from django.db import models
from django.core.validators import MinValueValidator


class Product(models.Model):
    name = models.CharField('Товар', max_length=30)
    price = models.DecimalField('Стоимость', max_digits=10, decimal_places=2,
                                default=0,
                                validators=[MinValueValidator(0)])
    category = models.ForeignKey('Category', related_name='products',
                                 on_delete=models.CASCADE, verbose_name='Категория')
    quantity_in_stock = models.PositiveIntegerField('Количество', default=0)
    reserve = models.PositiveIntegerField('Резерв', default=0)
    product_id = models.UUIDField('', default=uuid4, editable=False, unique=True)
    vendor_code = models.CharField('Артикул', max_length=30, null=True)
    description = models.TextField('Описание', blank=True)

    def __str__(self):
        return self.name

class Category(models.Model):
    name = models.CharField('Категория', max_length=50)
    main = models.BooleanField('Главная', )
    parent = models.ForeignKey(
        'self',
        on_delete=models.CASCADE,
        verbose_name='Родительская категория',
        blank=True,
        null=True
    )


    class Meta:
        ordering = ('name',)
        verbose_name = "Категория"
        verbose_name_plural = "Категории"

    def __str__(self):
        return self.name
