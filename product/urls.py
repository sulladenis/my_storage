from django.urls import path
from product import views


urlpatterns = [
    path('category/', views.category_create, name='category'),
    path('category_list/', views.category_list, name='category_list'),
    path('product/', views.product_create, name='product'),
    path('product_list/', views.product_list, name='product_list'),
    path('product_detail/<int:pk>/', views.product_detail, name='product_detail')
]
