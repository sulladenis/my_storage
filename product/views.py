from django.shortcuts import render, redirect, get_object_or_404
from product.forms import CategoryForm, ProductForm
from product.models import Category, Product
from django.core.paginator import EmptyPage, PageNotAnInteger, Paginator


def is_valid_queryparam(param):
    return param != '' and param is not None

def category_create(request):
    if request.method == 'POST':
        form = CategoryForm(request.POST)
        if form.is_valid():
            form.save()
            return redirect('category_list')
    else:
        form = CategoryForm()
    context = {'form': form}
    return render(request, 'category_create.html', context)

CAT_PAGE = 4

def filter_cat(request):
    qs = Category.objects.all()
    category_name_query = request.GET.get('name', '')
    print(category_name_query)
    category = request.GET.get('category')

    qs = qs.filter(name__icontains=category_name_query)

    if is_valid_queryparam(category) and category != 'Выбор...':
        qs = qs.filter(category__name=category)
    return qs


def category_list(request):

    context = {}
    # Search
    query = ""
    if request.GET:
        query = request.GET.get('name', '')
        query_cat = request.GET.get('category', '')
        context['query'] = str(query)
        context['query_cat'] = str(query_cat)

    category = filter_cat(request)
    print(category)

    page_number = request.GET.get('page', 1)
    category_paginator = Paginator(category, CAT_PAGE)
    try:
        category = category_paginator.page(page_number)
    except PageNotAnInteger:
        category = category_paginator.page(CAT_PAGE)
    except EmptyPage:
        category = category_paginator.page(category_paginator.num_pages)

    is_paginated = category.has_other_pages()

    context.update({
        'categories': Category.objects.all(),
        'category': category,
        'is_paginated': is_paginated
    })

    request.session['selector'] = 'category_list'

    return render(request, 'category_list.html', context)


def product_create(request):
    if request.method == 'POST':
        form = ProductForm(request.POST)
        if form.is_valid():
            form.save()
            return redirect('product_list')
    else:
        form = ProductForm()
    context = {'form': form}
    return render(request, 'product_create.html', context)



def filter(request):
    qs = Product.objects.all()
    categories = Category.objects.all()
    product_name_query = request.GET.get('name', '')
    category = request.GET.get('category')

    qs = qs.filter(name__icontains=product_name_query)

    if is_valid_queryparam(category) and category != 'Choose...':
        qs = qs.filter(category__name=category)

    return qs

PRODUCTS_PAGE = 6
def product_list(request):

    context = {}
    # Search
    query = ""
    if request.GET:
        query = request.GET.get('name', '')
        query_cat = request.GET.get('category', '')
        context['query'] = str(query)
        context['query_cat'] = str(query_cat)
        link = request.GET.get('link')
        if link == 'nav':
            request.session['selector'] = 'product_list'

    products = filter(request)

    page_number = request.GET.get('page', 1)
    products_paginator = Paginator(products, PRODUCTS_PAGE)
    try:
        products = products_paginator.page(page_number)
    except PageNotAnInteger:
        products = products_paginator.page(PRODUCTS_PAGE)
    except EmptyPage:
        products = products_paginator.page(products_paginator.num_pages)

    is_paginated = products.has_other_pages()
    context.update({
        'categories': Category.objects.all(),
        'products': products,
        'is_paginated': is_paginated
    })

    return render(request, 'product_list.html', context)

def product_detail(request, pk):
    product = get_object_or_404(Product, pk=pk)
    return render(request, 'product_detail.html', {'product': product})
