from django.urls import path
from storekeeper import views


urlpatterns = [
    path('supply_list/', views.supply_list, name='supply_list'),
    path('order_list/', views.order_list, name='order_list'),
    path('supply_detail_storekeeper/<int:pk>/', views.supply_detail_storekeeper, name='supply_detail_storekeeper'),
    path('order_detail_storekeeper/<int:pk>/', views.order_detail_storekeeper, name='order_detail_storekeeper'),
    path('deliver_to_storage/<int:pk>/', views.deliver_to_storage, name='deliver_to_storage'),
    path('shipment_from_stock/<int:pk>/', views.shipment_from_stock, name='shipment_from_stock'),
]
