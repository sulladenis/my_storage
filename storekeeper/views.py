from django.shortcuts import render, redirect
from supply.models import Supply
from order.models import Order
from supply.forms import SupplyForm
from order.forms import OrderForm
from django.contrib import messages


def index(request):
    return render(request, 'index.html')


def supply_list(request):
    supplies = Supply.objects.all()
    return render(request, 'supply_list.html', {'supplies': supplies})

def order_list(request):
    orders = Order.objects.all()
    return render(request, 'order_list.html', {'orders': orders})


def supply_detail_storekeeper(request, pk):
    supply = Supply.objects.get(pk=pk)
    supply_items = supply.supplyitem_set.all()
    if request.method == 'POST':
        form = SupplyForm(request.POST, instance=supply)
        if form.is_valid():
            form.save()
            return redirect('supply_list')
        else:
            messages.error(request, "Error")
    form = SupplyForm(instance=supply)
    context = {'form': form, 'supply': supply, 'supply_items': supply_items}
    return render(request, 'supply_detail_storekeeper.html', context)

def deliver_to_storage(request, pk):
    supply = Supply.objects.get(pk=pk)
    supply_items = supply.supplyitem_set.all()
    mes = 'Товары на склад не поступили'
    if supply.status != 1:
        supply.status = 1
        supply.save()
        for item in supply_items:
            item.accept()
        mes = 'Товары приняты на склад'

    context = {'mes': mes, 'supply': supply, 'supply_items': supply_items}
    return render(request, 'deliver_to_storage.html', context)

def order_detail_storekeeper(request, pk):
    order = Order.objects.get(pk=pk)
    order_items = order.orderitem_set.all()
    if request.method == 'POST':
        form = OrderForm(request.POST, instance=order)
        if form.is_valid():
            form.save()
            return redirect('order_list')
        else:
            messages.error(request, "Error")
    form = OrderForm(instance=order)
    context = {'form': form, 'order': order, 'order_items': order_items}
    return render(request, 'order_detail_storekeeper.html', context)

def shipment_from_stock(request, pk):
    order = Order.objects.get(pk=pk)
    order_items = order.orderitem_set.all()
    mes = 'Товары на склад не поступили'
    if order.status != 1:
        order.status = 1
        order.save()
        for item in order_items:
            item.accept()
        mes = 'Товары приняты на склад'

    context = {'mes': mes, 'order': order, 'order_items': order_items}
    return render(request, 'shipment_from_stock.html', context)
