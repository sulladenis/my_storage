from django.forms import ModelForm, TextInput, Select, NumberInput
from supply.models import Supplier, SupplyItem, Supply


class SupplyForm(ModelForm):
    class Meta:
        model = Supply
        fields = []


class SupplierForm(ModelForm):
    class Meta:
        model = Supplier
        fields = ['name']
        widgets = {
            'name': TextInput(
                attrs={
                    'class': 'form-control'
                }
            ),
        }
        labels = {
            'name': 'Введите имя поставщика'
        }


class SupplyItemForm(ModelForm):
    class Meta:
        model = SupplyItem
        fields = ['product', 'quantity']
        labels = {
            'product': 'Наименование товара',
            'quantity':'Количество',
        }
        widgets = {
            'product': Select(
                attrs={
                    'class': 'form-control',
                }
            ),
            'quantity': NumberInput(
                attrs={
                    'class': 'form-control'
                }
            ),
        }

