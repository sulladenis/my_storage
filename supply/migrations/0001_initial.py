# Generated by Django 3.0.3 on 2020-03-06 12:31

from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    initial = True

    dependencies = [
        ('product', '0001_initial'),
    ]

    operations = [
        migrations.CreateModel(
            name='Supplier',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('name', models.CharField(max_length=30, verbose_name='Поставщик')),
                ('contact_person', models.CharField(max_length=30, null=True, verbose_name='Контактное лицо')),
                ('email', models.EmailField(max_length=254, null=True, verbose_name='email')),
                ('phone', models.CharField(max_length=30, null=True, verbose_name='Телефон')),
            ],
        ),
        migrations.CreateModel(
            name='Supply',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('date', models.DateField(auto_now_add=True, verbose_name='Дата поставки')),
                ('status', models.IntegerField(choices=[(0, 'Поставка на рассмтрении'), (1, 'Товар поступил'), (2, 'Поставка не принята')], default=0, verbose_name='Статус')),
                ('supplier', models.ForeignKey(null=True, on_delete=django.db.models.deletion.CASCADE, to='supply.Supplier')),
            ],
        ),
        migrations.CreateModel(
            name='SupplyItem',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('quantity', models.PositiveIntegerField(default=1, verbose_name='Колличество')),
                ('product', models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, to='product.Product')),
                ('supply', models.ForeignKey(null=True, on_delete=django.db.models.deletion.CASCADE, to='supply.Supply')),
            ],
        ),
    ]
