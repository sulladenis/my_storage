from django.db import models
from product.models import Product


class Supplier(models.Model):
    name = models.CharField('Поставщик', max_length=30)
    contact_person = models.CharField('Контактное лицо', max_length=30, null=True)
    email = models.EmailField('email', null=True)
    phone = models.CharField('Телефон', max_length=30, null=True)

    def __str__(self):
        return self.name


class Supply(models.Model):
    STATUS_CHOICES = (
        (0, 'Поставка на рассмтрении'),
        (1, 'Товар поступил'),
        (2, 'Поставка не принята'),
    )

    date = models.DateField('Дата поставки', auto_now_add=True)
    supplier = models.ForeignKey(Supplier, null=True, on_delete=models.CASCADE)
    status = models.IntegerField("Статус", choices=STATUS_CHOICES, default=0)

    def __str__(self):
        return f'поставка {self.pk}'

    @property
    def total_price(self):
        set = self.supplyitem_set.all()
        price = 0
        for item in set:
            price += item.total_price_item
        return price


class SupplyItem(models.Model):
    product = models.ForeignKey(Product, on_delete=models.CASCADE)
    quantity = models.PositiveIntegerField('Колличество', default=1)
    supply = models.ForeignKey(Supply, null=True, on_delete=models.CASCADE)

    def accept(self):
        self.product.quantity_in_stock += self.quantity
        self.product.save()

    @property
    def total_price_item(self):
        return self.product.price * self.quantity




