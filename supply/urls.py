from django.urls import path
from supply import views


urlpatterns = [
    path('supplier/', views.supplier_create, name='supplier'),
    path('supplier_list', views.supplier_list, name='supplier_list'),
    path('supply_sent', views.supply_sent, name='supply_sent'),
    path('supply_detail/<int:pk>/', views.supply_detail, name='supply_detail'),
    path('product_detail_supply/<int:pk>/', views.product_detail_supply, name='product_detail_supply'),
    path('supplier_for_supply/<int:pk>/', views.supplier_for_supply, name='supplier_for_supply' )
]
