from django.shortcuts import render, redirect, get_object_or_404
from supply.forms import SupplierForm, SupplyItemForm, SupplyForm
from supply.models import Supplier, Supply
from product.models import Product


def supplier_create(request):
    # Если запрос на сервер (POST) - заполненная форма приходит на сервер
    if request.method == 'POST':
        # Присваиваем переменной form данные из запроса (обьек формы с данными)
        form = SupplierForm(request.POST)
        # Если данные из формы коректны (прошли валидацию)
        if form.is_valid():
            # Присваиваем переменной name данные из поля формы name
            name = form.cleaned_data['name']
            # Проверяем а есть ли уже такой поставщик
            try:
                # Если есть - присваиваем обьект (поставщик) переменной supplier
                supplier = Supplier.objects.get(name=name)
            # Если получаем исключение - (такого обьекта нет)
            except Supplier.DoesNotExist:
                # То сохраняем новый обьект в БД и передаем его переменной supplier
                supplier = form.save()
            # Теперь создаем новый обьект (поставка) и прикрепляем к нему поставщика
            supply = Supply.objects.create(supplier=supplier)
            # Переходим по адресу свежесозданной поставки *(передаем номер поставки - pk в url)
            return redirect('supply_detail', pk=supply.pk)
    # Иначе это первое обращение на страницу методом GET
    else:
        # Тогда мы обьект пустой формы (поставщик) присваеваем имя form
        form = SupplierForm()
    # Отправляем форму на страницу - ввода пользователя
    context = {'form': form}
    return render(request, 'supplier.html', context)


# В это представление вызывается из urls.py *(ему передается номер поставки pk из url)
def supply_detail(request, pk):
    request.session['selector'] = 'from_supply'
    # Получаем нужный обьект поставки (делаем её текущей)
    supply = get_object_or_404(Supply, pk=pk)
    request.session['supply'] = supply.pk
    # Вытаскиваем из обьекта поставки все "товарные позитции" (обьект - набор QuerySet в переменную)
    supply_items = supply.supplyitem_set.all()
    # Если запрос на сервер (POST) - заполненная форма приходит на сервер
    if request.method == 'POST':
        # Присваиваем переменной form данные из запроса (обьек формы с данными)
        form = SupplyItemForm(request.POST)
        # Если данные из формы коректны (прошли валидацию)
        if form.is_valid():
            # То сохраняем новый обьект (из формы в модель без записи в БД) и передаем его переменной order_item
            supply_item = form.save(commit=False)
            # Привязываем "товарную позицию" к текущей поставки
            supply_item.supply = supply
            # Сохраняем в базу данных "товарную позицию"
            supply_item.save()
            return redirect('supply_detail', pk)
    # В случае перехода на страницу в первый раз (тоесть без отправки формы на неё - метод GET)
    else:
        # Обьект пустой формы (Товарная позиция) присваеваем к переменной form
        form = SupplyItemForm()
    # Снабжаем словарь контекста обьектом формы, обьектом поставки, набором товарных позицый привязанных к поставке
    context = {'form': form, 'supply': supply, 'supply_items': supply_items}
    # И все это добро помещаем в страницу которую отправляем браузеру
    return render(request, 'supply_detail.html', context)


def product_list_supply(request):
    products = Product.objects.all()
    return render(request, 'product_list.html', {'products': products})


def product_detail_supply(request, pk):
    supply_pk = request.session.get('supply')
    supply = Supply.objects.get(pk=supply_pk)
    if request.method == 'POST':
        form = SupplyItemForm(request.POST)
        if form.is_valid():
            supply_item = form.save(commit=False)
            supply_item.supply = supply
            supply_item.save()
            return redirect('supply_detail', pk=supply_pk)
    else:
        product = Product.objects.get(pk=pk)
        form = SupplyItemForm(initial={'product': product})
    return render(request, 'product_detail_supply.html', {'form': form, 'supply': supply})


def supplier_list(request):
    suppliers = Supplier.objects.all()
    link = request.GET.get('link')
    if link == 'nav':
        request.session['selector'] = ''
    return render(request, 'supplier_list.html', {'suppliers': suppliers})


def supply_sent(request):
    supply_pk = request.session.get('supply')
    supply = Supply.objects.get(pk=supply_pk)
    del request.session['supply']
    del request.session['selector']
    return render(request, 'supply_sent.html', {'supply': supply})

def supplier_for_supply(request, pk):
    supply_pk = request.session.get('supply')
    supply = Supply.objects.get(pk=supply_pk)
    supplier = Supplier.objects.get(pk=pk)
    if request.method == 'POST':
        form = SupplyForm(request.POST, instance=supply)
        if form.is_valid():
            supply = form.save(commit=False)
            supply.supplier = supplier
            supply.save()
            return redirect('supply_detail', supply_pk)
    else:
        form = SupplyForm()
        context = {'form': form, 'supplier': supplier, 'supply': supply}
        return render(request, 'supplier_for_supply.html', context=context)
